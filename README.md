Random Node Load

* Description
An autocomplete form in a block that loads a random node.

### Features

* A block that includes:
  * A submit button that makes an ajax call to load the given node.
  * Load the nid's content into the body of the current page.


